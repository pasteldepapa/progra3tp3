package paqueteGoloso;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Random;

import paqueteGrafo.Grafo;
import paqueteGrafo.Vertice;

public class Goloso {
	private static ArrayList<Vertice> conjuntoDominante;
	private static ArrayList<Vertice> verticesGoloso = new ArrayList<Vertice>();
	
	

	public static ArrayList<Vertice> resolver(Grafo grafo) {
		
		validarGrafo(grafo);
		conjuntoDominante = new ArrayList<Vertice>();
		verticesGoloso = grafo.getVertices();
		Collections.sort(verticesGoloso, Collections.reverseOrder());
		agregarVerticesDeGradoCero(verticesGoloso);
		while (!esConjuntoDominante(grafo, conjuntoDominante))
		{
			Vertice candidato = elegirVerticeMayorGrado(verticesGoloso);
			conjuntoDominante.add(candidato);
			verticesGoloso.remove(verticesGoloso.indexOf(candidato));
		}
		
		return conjuntoDominante;
	}
	
	static void agregarVerticesDeGradoCero(ArrayList<Vertice> vertices) {
		validarVertices(vertices);
		for(Vertice vertice: vertices) {
			if(vertice.getGrado() == 0) {
				conjuntoDominante.add(vertice);
			}
		}
	}
	
	

	static boolean esConjuntoDominante(Grafo grafo, ArrayList<Vertice> conjuntoDominanteMinimo)
	{
		validarGrafo(grafo);
		validarVertices(conjuntoDominanteMinimo);
		HashSet<Vertice> verticesAlcanzadosDelConjuntoDominante = new HashSet<Vertice>();
		for(Vertice vertice: conjuntoDominanteMinimo)
		{	
			verticesAlcanzadosDelConjuntoDominante.add(vertice);
			verticesAlcanzadosDelConjuntoDominante.addAll(obtenerVecinosDelVertice(vertice, grafo));
		}
		return verticesAlcanzadosDelConjuntoDominante.size() == grafo.getSize();
		
	}
	
	static HashSet<Vertice> obtenerVecinosDelVertice(Vertice vertice, Grafo grafo) {
		validarGrafo(grafo);
		validarVertice(vertice);
		HashSet<Vertice> vecinos = new HashSet<Vertice>();
		for (Vertice v: grafo.getVertices()) 
		{
			if (grafo.getMatrizAdyacencia()[vertice.getId()][v.getId()]) {
				vecinos.add(v);
			}
		}
		return vecinos;
	}

	

	static Vertice elegirVerticeMayorGrado(ArrayList<Vertice> vertices)
	{
		validarVertices(vertices);
		ArrayList<Vertice> subGrupoDelMismoGrado = obtenerSubgrupoMayorGrado(vertices);
		
		return (subGrupoDelMismoGrado.size() > 1)? 
				elegirVerticeAlAzar(subGrupoDelMismoGrado)
				:subGrupoDelMismoGrado.get(0);
	}
	
	static ArrayList<Vertice> obtenerSubgrupoMayorGrado(ArrayList<Vertice> vertices) 
	{
		validarVertices(vertices);
		ArrayList<Vertice> subgrupo = new ArrayList<Vertice>();
		for (int i = 0; i < vertices.size(); i++)
		{
			if (vertices.get(0).getGrado() == vertices.get(i).getGrado())
			{
				subgrupo.add(vertices.get(i));
			}
		}
		return subgrupo;
	}



	static Vertice elegirVerticeAlAzar(ArrayList<Vertice> vertice)
	{
		
		Random rand = new Random();
        return vertice.get(rand.nextInt(vertice.size()));
	}
	
	static void validarGrafo(Grafo grafo) {
		if (grafo == null)
		{
			throw new IllegalArgumentException("El grafo no puedo ser null");
		}
		
	}
	
	 static void validarVertices(ArrayList<Vertice> vertices) {
		 if (vertices == null)
			{
				throw new IllegalArgumentException("Los vertices no puedo ser null");
			}
		 for(Vertice v: vertices) {
			 if (v == null)
				{
					throw new IllegalArgumentException("Uno de los vertices es null");
				}
		 }
		 
	}
	 private static void validarVertice(Vertice vertice) {
		 if (vertice == null)
			{
				throw new IllegalArgumentException("El vertice no puedo ser null");
			}
			
		}
}
