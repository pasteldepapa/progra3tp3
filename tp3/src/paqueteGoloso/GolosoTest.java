package paqueteGoloso;


import java.awt.Point;
import java.util.ArrayList;
import java.util.Collections;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import paqueteGrafo.Grafo;
import paqueteGrafo.Vertice;

public class GolosoTest {
	@Test(expected = IllegalArgumentException.class)
	public void resolverConMatrizVacia()
	{	
		Goloso.resolver(null);
	}
	
	@Test
	public void resolverOk()
	{
		Point arista1 = new Point(0,1);
		ArrayList<Point> aristas = new ArrayList<Point>();
		aristas.add(arista1);
		
		Grafo grafo = new Grafo(2, aristas);
		Goloso.resolver(grafo);
	}

	@Test(expected = IllegalArgumentException.class)
	public void agregarVDG0ConVerticesNull(){
		ArrayList<Vertice> vertices = new ArrayList<Vertice>();
		Vertice aux = null;
		vertices.add(aux);
		Goloso.agregarVerticesDeGradoCero(vertices);
		}
	
	
	@Test(expected = IllegalArgumentException.class)
	public void esConjuntoDominanteConGrafoNull(){
	ArrayList<Vertice> vertices = new ArrayList<Vertice>();
	Goloso.esConjuntoDominante(null,vertices);
	}

	@Test(expected = IllegalArgumentException.class)
	public void esConjuntoDominanteConUnVerticeNull(){
		Point arista1 = new Point(0,1);
		Point arista2 = new Point(1,0);
		ArrayList<Point> aristas = new ArrayList<Point>();
		aristas.add(arista1);
		aristas.add(arista2);
		
		Grafo grafo = new Grafo(2, aristas);
		ArrayList<Vertice> vertices = new ArrayList<Vertice>();
		Vertice aux = null;
		Vertice verticeReal = new Vertice(1,0,0,0);
		vertices.add(verticeReal);
		vertices.add(aux);
		Goloso.esConjuntoDominante(grafo,vertices);
	}

	@Test 
	public void esConjuntoDominanteFalso(){
		Point arista1 = new Point(0,1);
		Point arista2 = new Point(1,2);
		ArrayList<Point> aristas = new ArrayList<Point>();
		aristas.add(arista1);
		aristas.add(arista2);
		Grafo grafo = new Grafo(3, aristas);
		
		Vertice aux = new Vertice (2,1,0,0);
		ArrayList<Vertice> aux2 = new ArrayList<Vertice>();
		aux2.add(aux);
		assertFalse(Goloso.esConjuntoDominante(grafo,aux2));
	}
	
	@Test
	public void esConjuntoDominanteOk() {
		Point arista1 = new Point(0,1);
		Point arista2 = new Point(1,2);
		ArrayList<Point> aristas = new ArrayList<Point>();
		aristas.add(arista1);
		aristas.add(arista2);
		Grafo grafo = new Grafo(3, aristas);
		
		Vertice aux = new Vertice (1,2,0,0);
		ArrayList<Vertice> aux2 = new ArrayList<Vertice>();
		aux2.add(aux);
		assertTrue(Goloso.esConjuntoDominante(grafo,aux2));
	}


	@Test(expected = IllegalArgumentException.class)
	public void obtenerVecinosDelVerticeConGrafoNull(){
		Grafo grafo = null;
		Vertice aux = new Vertice (1,2,0,0);
		
		 Goloso.obtenerVecinosDelVertice(aux, grafo);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void obtenerVecinosDelVerticeConVerticesNull(){
		Point arista1 = new Point(0,1);
		Point arista2 = new Point(1,2);
		ArrayList<Point> aristas = new ArrayList<Point>();
		aristas.add(arista1);
		aristas.add(arista2);
		Grafo grafo = new Grafo(3, aristas);
		Vertice aux = null;
		
		 Goloso.obtenerVecinosDelVertice(aux, grafo);
	}
	
	
	@Test(expected = IllegalArgumentException.class)
	public void elegirVerticeMayorGradoConVerticesNull(){
		ArrayList<Vertice> vertices = null; 
		Goloso.elegirVerticeMayorGrado(vertices);
	}
	
	@Test
	public void elegirVerticeMayorGradoFalso(){
		ArrayList<Vertice> vertices = new ArrayList<Vertice>(); 
		Vertice aux1 = new Vertice(0,1,0,0);
		Vertice aux2 = new Vertice(1,2,0,0);
		Vertice aux3 = new Vertice(2,1,0,0);
		vertices.add(aux1);
		vertices.add(aux2);
		vertices.add(aux3);
		Collections.sort(vertices, Collections.reverseOrder());
		assertNotEquals(Goloso.elegirVerticeMayorGrado(vertices),(aux1));
	}
	
	@Test
	public void elegirVerticeMayorGradoOk(){
		ArrayList<Vertice> vertices = new ArrayList<Vertice>(); 
		Vertice aux1 = new Vertice(0,1,0,0);
		Vertice aux2 = new Vertice(1,2,0,0);
		Vertice aux3 = new Vertice(2,1,0,0);
		vertices.add(aux1);
		vertices.add(aux2);
		vertices.add(aux3);
		Collections.sort(vertices, Collections.reverseOrder());
		assertEquals(Goloso.elegirVerticeMayorGrado(vertices),(aux2));
	}

	@Test
	public void obtenerSubgrupoMayorGradoConVerticesNull(){
		ArrayList<Vertice> vertices = new ArrayList<Vertice>(); 
		Vertice aux1 = new Vertice(0,1,0,0);
		Vertice aux2 = new Vertice(1,2,0,0);
		Vertice aux3 = new Vertice(2,1,0,0);
		vertices.add(aux1);
		vertices.add(aux2);
		vertices.add(aux3);
		Collections.sort(vertices, Collections.reverseOrder());
	}
	@Test
	public void obtenerSubgrupoMayorGradoFalso(){
		ArrayList<Vertice> vertices = new ArrayList<Vertice>(); 
		Vertice aux1 = new Vertice(0,1,0,0);
		Vertice aux2 = new Vertice(1,2,0,0);
		Vertice aux3 = new Vertice(2,1,0,0);
		vertices.add(aux1);
		vertices.add(aux2);
		vertices.add(aux3);
		Collections.sort(vertices, Collections.reverseOrder());
		ArrayList<Vertice> vertices2 = new ArrayList<Vertice>();
		vertices2.add(aux1);
		assertNotEquals(Goloso.obtenerSubgrupoMayorGrado(vertices),(vertices2));
	}
	
	@Test
	public void obtenerSubgrupoMayorGradoOk(){
		ArrayList<Vertice> vertices = new ArrayList<Vertice>(); 
		Vertice aux1 = new Vertice(0,1,0,0);
		Vertice aux2 = new Vertice(1,2,0,0);
		Vertice aux3 = new Vertice(2,1,0,0);
		vertices.add(aux1);
		vertices.add(aux2);
		vertices.add(aux3);
		Collections.sort(vertices, Collections.reverseOrder());
		ArrayList<Vertice> vertices2 = new ArrayList<Vertice>();
		vertices2.add(aux2);
		assertEquals(Goloso.obtenerSubgrupoMayorGrado(vertices),(vertices2));
	}

}