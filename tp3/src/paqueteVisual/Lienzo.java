package paqueteVisual;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;

import javax.swing.JPanel;

import paqueteGrafo.Vertice;

public class Lienzo extends JPanel implements MouseListener {
    private ArrayList<Vertice> vertices;
    private ArrayList<Point> aristas;
    private ArrayList<Vertice> conjuntoMinimo;

    public Lienzo() {
        vertices = new ArrayList<Vertice>();
        aristas = new ArrayList<Point>();
        this.addMouseListener(this);
    }

    public void agregarVertice(Vertice v) {
        vertices.add(v);
    }
    
    public void agregarArista(Point arista) {
    	aristas.add(arista);
    }

    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        // Dibuja los vértices en el lienzo
        for (Vertice vertice : vertices) 
        {
          if (this.conjuntoMinimo != null) 
          {
        	  for(Vertice v: conjuntoMinimo) {
        		  if (v.getId() == vertice.getId()) 
        		  {
        			  g.setColor(Color.cyan); // Establece el color de dibujo a rojo
        			 // g.drawOval((int) vertice.getCoord_X() - 35, (int) vertice.getCoord_Y() - 35, 65, 65);
        			  g.fillOval((int)vertice.getCoord_X() - 30, (int)vertice.getCoord_Y() - 30, 60, 60);
        		  }
        	  }
          }
          
          g.setColor(Color.black);
          g.drawOval((int)vertice.getCoord_X() - 30, (int)vertice.getCoord_Y() - 30, 60, 60);
         // Dibuja el ID del vértice adentro del círculo
          g.drawString(Integer.toString(vertice.getId()), (int)vertice.getCoord_X() -3, (int)vertice.getCoord_Y() +5 );
        }
     // Dibuja las aristas
        for (Point arista : aristas) {
            int idVerticeA = arista.x;
            int idVerticeB = arista.y;

            Vertice verticeA = buscarVerticePorId(idVerticeA);
            Vertice verticeB = buscarVerticePorId(idVerticeB);

            if (verticeA != null && verticeB != null) {
                int x1 = (int) verticeA.getCoord_X();
                int y1 = (int) verticeA.getCoord_Y();
                int x2 = (int) verticeB.getCoord_X();
                int y2 = (int) verticeB.getCoord_Y();
                g.drawLine(x1, y1, x2, y2);
                
            }
        }
     
    }
    
    @Override
    public void mouseClicked(MouseEvent e) {
    }

    @Override
    public void mousePressed(MouseEvent e) {
    }

    @Override
    public void mouseReleased(MouseEvent e) {
    }

    @Override
    public void mouseEntered(MouseEvent e) {
    }

    @Override
    public void mouseExited(MouseEvent e) {
    }
    
    private Vertice buscarVerticePorId(int id) {
        for (Vertice vertice : vertices) {
            if (vertice.getId() == id) {
                return vertice;
            }
        }
        return null; // Devuelve null si no se encuentra el vértice
    }

	public void agregarConjuntoMinimo(ArrayList<Vertice> conjuntoDomMinimo) {
		this.conjuntoMinimo = conjuntoDomMinimo;
		
	}
}