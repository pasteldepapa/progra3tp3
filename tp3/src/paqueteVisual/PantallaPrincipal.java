package paqueteVisual;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;


import paqueteGoloso.Goloso;
import paqueteGrafo.Grafo;
import paqueteGrafo.Vertice;

public class PantallaPrincipal  {

	private JFrame frame;
	public ArrayList<Point> aristas;
	private ArrayList<Vertice> vertices;

	private Lienzo lienzo;
	private int verticeCounter = 0;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					PantallaPrincipal window = new PantallaPrincipal();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public PantallaPrincipal() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame("Pantalla Principal");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setSize(800, 600);

		// Crear un JPanel para los botones en la parte inferior
		JPanel buttonPanel = new JPanel();
		frame.getContentPane().add(buttonPanel, BorderLayout.SOUTH);
		vertices = new ArrayList<Vertice>();
		aristas = new ArrayList<Point>();
		lienzo = new Lienzo();
		frame.add(lienzo);

		agregarBotones(buttonPanel, lienzo);
		frame.setVisible(true);

	}

	private void agregarBotones(JPanel buttonPanel, Lienzo lienzo) {

		JButton agregarVerticeButton = new JButton("Agregar Vertice");
		agregarVerticeButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (verticeCounter < 9) {
					Vertice aux = crearVerticeConCoordenadas();
					vertices.add(aux);
					lienzo.agregarVertice(aux);
					lienzo.repaint();
				} else {
					mostrarMensaje("La cantidad de vertices es 9");
				}
			}
		});
		buttonPanel.add(agregarVerticeButton);

		JButton agregarAristaButton = new JButton("Agregar Arista");
		agregarAristaButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				IngresarAristaDialog dialog = new IngresarAristaDialog(frame, vertices.size());
				dialog.setVisible(true);
				Point aristaIngresada = dialog.getAristaNueva();
				if (aristaIngresada != null) {
					if (!aristaExistente(aristaIngresada)) {
						aristas.add(aristaIngresada);
						lienzo.agregarArista(aristaIngresada);
						lienzo.repaint();
					} else {
						mostrarMensaje("Esta arista ya existe.");
					}
				}
			}

		});
		buttonPanel.add(agregarAristaButton);

		JButton mostrarConjuntoDominanteButton = new JButton("Mostrar Conjunto Dominante");
		mostrarConjuntoDominanteButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
					Grafo grafo = new Grafo(vertices.size(),aristas);
					ArrayList<Vertice> conjuntoDomMinimo = Goloso.resolver(grafo);
					
					lienzo.agregarConjuntoMinimo(conjuntoDomMinimo);
					lienzo.repaint();
					
			}
		});
		buttonPanel.add(mostrarConjuntoDominanteButton);

	}

	private Vertice crearVerticeConCoordenadas() {

		int radio = 200;
		double anguloDeRotacion = 2 * Math.PI / 9;
		double anguloInicial = -Math.PI / 2;
		int resto = verticeCounter % 9;
		double coord_X = radio * Math.cos(anguloInicial + anguloDeRotacion * resto) + 400;
		double coord_Y = radio * Math.sin(anguloInicial + anguloDeRotacion * resto) + 270;
		Vertice aux = new Vertice(verticeCounter, coord_X, coord_Y);
		verticeCounter++;
		return aux;
	}

	private boolean aristaExistente(Point aristaIngresada) {
		boolean ret = false;
		for (Point arista : this.aristas) {
			ret = ret || (arista.getX() == aristaIngresada.getX() && arista.getY() == aristaIngresada.getY())
					|| (arista.getY() == aristaIngresada.getX() && arista.getX() == aristaIngresada.getY());
		}
		return ret;
	}

	private void mostrarMensaje(String mensaje) {
		JOptionPane.showMessageDialog(frame, mensaje);
	}

}