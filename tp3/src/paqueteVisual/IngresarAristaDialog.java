package paqueteVisual;

import java.awt.Frame;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.JTextField;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JButton;

public class IngresarAristaDialog extends JDialog {
	private JTextField nodoA_txtFld;
	private JTextField nodoB_txtFld;
	private Point aristaNueva;

	private int cantVertices;

	public IngresarAristaDialog(Frame parent, int cantVertices) {
		super(parent, "Ingresar Arista", true);
		this.cantVertices = cantVertices;
		setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		setSize(350, 250);
		setLocationRelativeTo(parent);
		getContentPane().setLayout(null);
		aristaNueva = null;

		nodoA_txtFld = new JTextField();
		nodoA_txtFld.setBounds(112, 60, 86, 20);
		getContentPane().add(nodoA_txtFld);
		nodoA_txtFld.setColumns(10);

		nodoB_txtFld = new JTextField();
		nodoB_txtFld.setBounds(112, 91, 86, 20);
		getContentPane().add(nodoB_txtFld);
		nodoB_txtFld.setColumns(10);

		JLabel textLbl = new JLabel("Ingrese el numero de los vertices que sea unir:");
		textLbl.setBounds(23, 23, 300, 14);
		getContentPane().add(textLbl);

		JLabel nodoALbl = new JLabel("Nodo A:");
		nodoALbl.setBounds(36, 63, 78, 14);
		getContentPane().add(nodoALbl);

		JLabel NodoBLbl = new JLabel("Nodo B:");
		NodoBLbl.setBounds(36, 94, 78, 14);
		getContentPane().add(NodoBLbl);

		crearBotonUnit();
		JPanel panel = new JPanel();
		panel.setLayout(null);

	}

	private void crearBotonUnit() {

		JButton btnUnir = new JButton("UNIR");
		btnUnir.setBounds(83, 137, 89, 23);
		btnUnir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				if (nodoB_txtFld.getText().isEmpty() && nodoA_txtFld.getText().isEmpty()) {
					mostrarMensaje("Los campos no pueden estar vacío.");
				} else {

					if ((contieneSoloNumeros(nodoA_txtFld.getText()) && contieneSoloNumeros(nodoB_txtFld.getText()))
							&& (nodoExistente(Integer.parseInt((String) nodoA_txtFld.getText()))
									&& nodoExistente(Integer.parseInt((String) nodoB_txtFld.getText())))
							&& (Integer.parseInt((String) nodoA_txtFld.getText()) != Integer
									.parseInt((String) nodoB_txtFld.getText()))) {

						aristaNueva = new Point(Integer.parseInt((String) nodoA_txtFld.getText()),
								Integer.parseInt((String) nodoB_txtFld.getText()));
						dispose();
					} else {
						mostrarMensaje(
								"Solo se puede ingresar numeros, que se encuentren utilizados en los vertices existentes y que no sea el mismo nodo");
					}
				}
			}

		});

		getContentPane().add(btnUnir);
	}

	private void mostrarMensaje(String mensaje) {
		JOptionPane.showMessageDialog(this, mensaje);
	}

	private boolean contieneSoloNumeros(String input) {
		String regex = "^-?\\d+(\\.\\d+)?$";

		Pattern pattern = Pattern.compile(regex);
		Matcher matcher = pattern.matcher(input);

		return matcher.matches();
	}

	private boolean nodoExistente(int numero) {
		return numero >= 0 & numero < this.cantVertices;
	}

	public Point getAristaNueva() {
		return aristaNueva;
	}

}