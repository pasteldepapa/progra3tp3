package paqueteGrafo;

import java.awt.Point;
import java.util.ArrayList;

public class Grafo
{
	// Representamos el grafo por su matriz de adyacencia
	private static boolean[][] matriz;
	private ArrayList<Point> aristas;
	private ArrayList<Vertice> vertices;

	
	// La cantidad de vertices esta predeterminada desde el constructor
	public Grafo(int cantidadDeVertices, ArrayList<Point> aristas)
	{
		verificarCantidadDeVertices(cantidadDeVertices);
		verificarAristas(cantidadDeVertices,aristas);
		this.aristas = aristas;
		matriz = construirMatrizAdyacencia(cantidadDeVertices, this.aristas);
		this.vertices = crearVertices(matriz);
	}
	


	boolean[][] construirMatrizAdyacencia(int cantidadDeVertices, ArrayList<Point> aristas) 
	{
		
		verificarCantidadDeVertices(cantidadDeVertices);
		verificarAristas(cantidadDeVertices, aristas);
		
		boolean [][] ret = new boolean[cantidadDeVertices][cantidadDeVertices];
		
		if (aristas != null)
		{
			for (Point arista: aristas)
			{
				ret[arista.x][arista.y] = true;
				ret[arista.y][arista.x] = true;
			}
		}
		
		return ret;
	}

	ArrayList<Vertice> crearVertices(boolean[][] matriz) 
	{
		verificarMatriz(matriz);
		
		ArrayList<Vertice> ret = new ArrayList<Vertice>(); 
		for(int i = 0; i < matriz.length; i++) 
		{
			ret.add(new Vertice(i, grado(i), 0, 0));
		}
		return ret;
	}

	
	// Vecinos de un vertice
	public int grado(int i)
	{
		verificarVertice(i);
		int ret = 0;
		
		for (int columna = 0; columna < matriz.length; columna++)
		{
			if (matriz[i][columna])
			{
				ret += 1;
			}
		}
		
		return ret;
	}
	
	// Verifica que sea un vertice valido
	private void verificarCantidadDeVertices(int i)
	{
		if( i <= 0 )
			throw new IllegalArgumentException("La cantidad de vertices no puede ser negativa ni cero: " + i);
		
		if( i >= 10 )
			throw new IllegalArgumentException("La cantidad de vertices no puede ser superior a 9: " + i);
	}
	
	// Verifica que no existan aristas repetidas
	private void verificarAristas(int cantidadDeVertices, ArrayList<Point> aristas) 
	{
		int cantidadAristas = 0;
		if (aristas != null)
		{
			cantidadAristas = aristas.size();
		}
		
		int cantidadMaximaDeAristas = cantidadDeVertices * (cantidadDeVertices - 1) / 2;
		
		if (cantidadAristas > cantidadMaximaDeAristas)
		{
			throw new IllegalArgumentException("El grafo no puede tener mas aristas que Kn");
		}
		
		if(hayAristasRepetidas(aristas))
		{
			throw new IllegalArgumentException("No pueden haber aristas repetidas");
		}
	}
	
	private boolean hayAristasRepetidas(ArrayList<Point> aristas) {
		for (Point arista: aristas)
		{
			Point reverso = new Point(arista.y, arista.x);
			if (aristas.contains(reverso))
			{
				return true;
			}
		}
		return false;
	}
	
	// Verifica que sea un vertice valido
	private void verificarVertice(int i)
	{
		if( i < 0 )
			throw new IllegalArgumentException("El vertice no puede ser negativo: " + i);
		
		if( i >= matriz.length )
			throw new IllegalArgumentException("Los vertices deben estar entre 0 y |V|-1: " + i);
	}
	
	private void verificarMatriz(boolean[][] matriz) {
		if (matriz == null)
		{
			throw new IllegalArgumentException("La matriz no puede ser nula");
		}
	}


	public boolean[][] getMatrizAdyacencia()
	{
		return matriz;
	}
	
	public ArrayList<Vertice> getVertices()
	{
		return this.vertices;
	}
	
	public ArrayList<Point> getAristas()
	{
		return this.aristas;
	}
	
	public int getSize()
	{
		return matriz.length;
	}
	
}

