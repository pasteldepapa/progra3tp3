package paqueteGrafo;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

import java.awt.Point;
import java.util.ArrayList;

import org.junit.Test;

public class GrafoTest {
	@Test(expected = IllegalArgumentException.class)
	public void grafoConVerticesNegativos()
	{
		ArrayList<Point> aristas = new ArrayList<Point>();
		
		Grafo grafo = new Grafo(-1, aristas);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void grafoConCeroVertices()
	{
		ArrayList<Point> aristas = new ArrayList<Point>();
		
		Grafo grafo = new Grafo(0, aristas);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void grafoConAristasRepetidas()
	{
		Point arista1 = new Point(0,1);
		Point arista2 = new Point(1,0);
		ArrayList<Point> aristas = new ArrayList<Point>();
		aristas.add(arista1);
		aristas.add(arista2);
		
		Grafo grafo = new Grafo(2, aristas);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void grafoConMuchasAristas()
	{
		
		Point arista1 = new Point(0,1);
		Point arista2 = new Point(0,2);
		ArrayList<Point> aristas = new ArrayList<Point>();
		aristas.add(arista1);
		aristas.add(arista2);
		
		Grafo grafo = new Grafo(1, aristas);
	}
	
	@Test
	public void grafoOk()
	{
		Point arista1 = new Point(0,1);
		ArrayList<Point> aristas = new ArrayList<Point>();
		aristas.add(arista1);
		
		Grafo grafo = new Grafo(2, aristas);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void crearVerticeConMatrizNull()
	{
		ArrayList<Point> aristas = new ArrayList<Point>();
		Grafo grafo = new Grafo(1, aristas);
		
		grafo.crearVertices(null);
	}
	
	@Test
	public void crearVerticeOk()
	{
		boolean [][] matrizEsperada =  {{false, true, true},
								{true, false, false},
								{true, false, false}};
		
		
		Point arista1 = new Point(0,1);
		Point arista2 = new Point(0,2);
		ArrayList<Point> aristas = new ArrayList<Point>();
		aristas.add(arista1);
		aristas.add(arista2);
		
		Grafo grafo = new Grafo(3, aristas);
		
		assertArrayEquals(matrizEsperada, grafo.getMatrizAdyacencia());
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void consultarGradoVerticeInvalido()
	{
		Point arista1 = new Point(0,1);
		Point arista2 = new Point(0,2);
		ArrayList<Point> aristas = new ArrayList<Point>();
		aristas.add(arista1);
		aristas.add(arista2);
		
		Grafo grafo = new Grafo(3, aristas);
		grafo.grado(3);
	}
	
	@Test
	public void consultarGradoVerticeFalso()
	{
		Point arista1 = new Point(0,1);
		Point arista2 = new Point(0,2);
		ArrayList<Point> aristas = new ArrayList<Point>();
		aristas.add(arista1);
		aristas.add(arista2);
		
		Grafo grafo = new Grafo(3, aristas);
		
		assertNotEquals(3, grafo.grado(0));
	}
	
	@Test
	public void consultarGradoVerticeOk()
	{
		Point arista1 = new Point(0,1);
		Point arista2 = new Point(0,2);
		ArrayList<Point> aristas = new ArrayList<Point>();
		aristas.add(arista1);
		aristas.add(arista2);
		
		Grafo grafo = new Grafo(3, aristas);
		
		assertEquals(2, grafo.grado(0));
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void construirMatrizAdyacenciaConVerticesInvalidos()
	{
		Point arista1 = new Point(0,1);
		Point arista2 = new Point(0,2);
		ArrayList<Point> aristas = new ArrayList<Point>();
		aristas.add(arista1);
		aristas.add(arista2);
		
		Grafo grafo = new Grafo(3, aristas);
		grafo.construirMatrizAdyacencia(-1, aristas);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void construirMatrizAdyacenciaConAristasInvalidas()
	{
		Point arista1 = new Point(0,1);
		Point arista2 = new Point(2,2);
		ArrayList<Point> aristas = new ArrayList<Point>();
		aristas.add(arista1);
		aristas.add(arista2);
		
		Grafo grafo = new Grafo(3, aristas);
		grafo.construirMatrizAdyacencia(3, aristas);
	}
	
	@Test
	public void construirMatrizAdyacenciaFalse()
	{
		boolean [][] matrizErronea =  {{true, true, true},
										{true, false, false},
										{true, false, false}};


		Point arista1 = new Point(0,1);
		Point arista2 = new Point(0,2);
		ArrayList<Point> aristas = new ArrayList<Point>();
		aristas.add(arista1);
		aristas.add(arista2);
		
		Grafo grafo = new Grafo(3, aristas);
		
		assertNotEquals(matrizErronea, grafo.construirMatrizAdyacencia(3, aristas));
	}
	
	@Test
	public void construirMatrizAdyacenciaOk()
	{
		boolean [][] matrizErronea =  {{false, true, true},
										{true, false, false},
										{true, false, false}};


		Point arista1 = new Point(0,1);
		Point arista2 = new Point(0,2);
		ArrayList<Point> aristas = new ArrayList<Point>();
		aristas.add(arista1);
		aristas.add(arista2);
		
		Grafo grafo = new Grafo(3, aristas);
		
		assertArrayEquals(matrizErronea, grafo.construirMatrizAdyacencia(3, aristas));
	}

}
