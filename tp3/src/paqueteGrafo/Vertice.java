package paqueteGrafo;

public class Vertice implements Comparable<Vertice>{
	private Integer id;
	private Integer grado;
	private double coord_X;
	private double coord_Y;
	
	public Vertice(int id, int grado, double coord_X, double coord_Y) {
		validarParametros(id, grado,  coord_X,  coord_Y);
		this.id = id;
		this.grado = grado;
		this.coord_X = coord_X;
		this.coord_Y = coord_Y;
	}

	public Vertice(int id, double coord_X, double coord_Y) {
		this.grado = 0;
		validarParametros(id, this.grado,  coord_X,  coord_Y);
		this.id = id;
		this.coord_X = coord_X;
		this.coord_Y = coord_Y;
	}
	
	public double getCoord_X() {
		return coord_X;
	}

	public double getCoord_Y() {
		return coord_Y;
	}

	public Integer getId() {
		return id;
	}

	public Integer getGrado() {
		return grado;
	}

	@Override
	public int compareTo(Vertice v) 
	{
		return Integer.compare(this.grado, v.getGrado());
	}
	
	@Override
    public String toString() {
        return "Vertice:"+ getId() +"-Grado: "+ getGrado();
    }
	private void validarParametros(int id, int grado, double coord_X, double coord_Y) {
		if (id <0 || grado<0) {
			throw new IllegalArgumentException("El Id y/o Grado no puedo ser negativos");
		}
		
	}
	
}
